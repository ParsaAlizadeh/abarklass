months = {
  _names: [
    "farvardin", "ordibehesht", "khordad",
    "tir", "mordad", "shahrivar",
    "mehr", "aban", "azar",
    "dey", "bahman", "esfand"
  ],
  name(index) {
    return this._names[index];
  },
  number(name) {
    for (let i = 0; i < this._names.length; i++) {
      if (name == this._names[i])
        return i;
    }
  }
}

function populateSample1(data) {
  const ul = $("#sample1");
  for (const obj of data) {
    const li = $("<li></li>")
    const link = $("<a></a>")
    link.text(obj.name);
    link.attr("href", obj.href);
    li.append(link);
    ul.append(li);
  }
}

function populateSample2(data) {
  function build(parent, data) {
    if (Array.isArray(data)) {
      const innerul = $("<ul></ul>");
      for (const obj of data)
        build(innerul, obj);
      parent.append(innerul);
    } else {
      const li = $("<li></li>")
      const link = $("<a></a>")
      link.text(data.name);
      link.attr("href", data.href);
      li.append(link);
      parent.append(li);
    }
  }
  build($("#sample2"), data);
}

$(document).ready(function() {
  $.getJSON("/sample1.json", populateSample1);
  $.getJSON("/sample2.json", populateSample2);
})