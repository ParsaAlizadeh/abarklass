django~=4.0.5
gunicorn~=20.1.0
psycopg2-binary~=2.9.3
pylibmc~=1.6.1
tzdata
python-dotenv~=0.20.0
