import logging
from datetime import timedelta

from django.contrib.auth import login, logout
from django.core.paginator import Paginator
from django.shortcuts import get_object_or_404, redirect, render
from django.utils import timezone

from .forms import AddTaskForm, LoginRequestForm, LoginResolveForm, SignupForm
from .models import OTP, Task, TaskUser, User
from .decorators import (
    require_GET, require_POST,
    require_http_methods,
    require_full_signup_or_none,
    require_incomplete_signup,
    require_is_master,
    require_not_authenticated
)

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

def send_otp(email):
    user = User.objects.filter(username=email).first()
    if user is None:
        user = User.objects.create_user(username=email, email=email)
    otp = OTP.get_valids(user).first()
    if otp is None:
        otp = OTP.objects.create(
            user=user,
            password=User.objects.make_random_password(),
            expired=False
        )
    logger.info('send otp [user=%s, otp=%s]', user.username, otp.password)

@require_GET
@require_full_signup_or_none
def index(request):
    context = {
        'error': request.session.pop('error', None),
        'success': request.session.pop('success', None)
    }
    tasks = (
        Task.objects
        .select_related()
        .filter(deadline__gte=timezone.now())
        .order_by('deadline')
    )
    pagniator = Paginator(tasks, 10)
    page_num = request.GET.get('page') or request.session.get('page_num', 1)
    request.session['page_num'] = page_num
    context['page'] = pagniator.get_page(page_num)

    return render(request, 'task/index.html', context)

@require_GET
@require_full_signup_or_none
def details(request, task_id):
    context = {}
    context['task'] = get_object_or_404(Task, pk=task_id)
    taskuser = TaskUser.from_user(request.user)
    context['can_do'] = taskuser is not None and not taskuser.is_master
    return render(request, 'task/details.html', context)

@require_POST
@require_not_authenticated
def login_request_view(request):
    form = LoginRequestForm(request.POST)
    if not form.is_valid():
        request.session['error'] = form.errors.as_ul()
        return redirect('task:index')
    email = form.cleaned_data['email']
    send_otp(email)
    request.session['success'] = 'ایمیل ورود ارسال شد'
    return redirect('task:index')

@require_POST
@require_not_authenticated
def login_resolve_view(request):
    form = LoginResolveForm(request.POST)
    if not form.is_valid():
        request.session['error'] = form.errors.as_ul()
        return redirect('task:index')
    password = form.cleaned_data['password']
    otp = OTP.objects.filter(password=password).first()
    if otp is None:
        logger.warning('no such otp [otp=%s]', password)
        request.session['error'] = 'لینک ورود اشتباه است'
        return redirect('task:index')
    valid_otps = OTP.get_valids(otp.user)
    if otp not in valid_otps:
        logger.warning('not a valid otp [otp=%s]', otp.password)
        request.session['error'] = 'لینک ورود منقضی شده است'
        return redirect('task:index')
    valid_otps.update(expired=True)
    login(request, otp.user)
    if TaskUser.from_user(otp.user) is not None:
        request.session['success'] = 'با موفقیت وارد شدید'
        return redirect('task:index')
    return render(request, 'task/signup.html')

@require_http_methods(['GET', 'POST'])
@require_incomplete_signup
def signup_view(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if not form.is_valid():
            context = {
                'error': form.errors.as_ul(),
                'form': form
            }
            return render(request, 'task/signup.html', context=context)
        cleaned_data = form.cleaned_data
        user = request.user
        user.first_name = cleaned_data['firstname']
        user.last_name = cleaned_data['lastname']
        user.save()
        taskuser = TaskUser.objects.create(
            user=user, is_master=cleaned_data['is_master']
        )
        request.session['success'] = 'اطلاعات با موفقیت ثبت شد'
        return redirect('task:index')
    return render(request, 'task/signup.html')

@require_POST
def logout_view(request):
    logout(request)
    request.session['success'] = 'با موفقیت خارج شدید'
    return redirect('task:index')

@require_http_methods(['GET', 'POST'])
@require_full_signup_or_none
@require_is_master
def add_task(request):
    context = {}
    if request.method == 'POST':
        form = AddTaskForm(request.POST)
        if form.is_valid():
            task = form.save(commit=False)
            task.author = TaskUser.from_user(request.user)
            task.save()
            return redirect('task:details', task_id=task.pk)
        context['form'] = form
    return render(request, 'task/add_task.html', context)
