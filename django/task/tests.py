import datetime

from django.test import TestCase
from django.utils import timezone
from django.urls import reverse

from .models import User, TaskUser, Task, OTP

class OTPTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username='seth@example.org',
            email='seth@example.org'
        )
        unused_user = User.objects.create_user(
            username='john@example.net',
            email='john@example.net'
        )
        self.old_otp = OTP.objects.create(
            user=self.user,
            created_at=timezone.now() - datetime.timedelta(hours=12),
            password='5ovmmWfZzg',
            expired=True
        )

    def post_email(self, email):
        return self.client.post(
            reverse('task:login-request'),
            data={'email': email},
            follow=True
        )

    def post_password(self, password):
        return self.client.post(
            reverse('task:login-resolve'),
            data={'password': password},
            follow=True
        )

    def try_bad_email_login(self):
        resp = self.post_email('@example.org')
        self.assertIsNotNone(resp.context.get('error'), 'it doesnt return error')

    def try_login(self):
        resp = self.post_email(self.user.email)
        self.assertEqual(resp.status_code, 200, 'login with valid email is not successful')
        self.assertIsNone(resp.context.get('error'), 'it returns an error')
        self.assertIsNotNone(resp.context.get('success'), 'no success message')

    def get_generated_otp(self):
        otps = OTP.objects.filter(expired=False)
        self.assertEqual(otps.count(), 1, 'no new otp created')
        return otps[0]

    def check_new_otp(self, otp):
        self.assertNotEqual(
            otp.password, self.old_otp.password,
            'password is the same as old one'
        )

    def try_old_otp(self):
        resp = self.post_password(self.old_otp.password)
        self.assertIsNotNone(resp.context['error'], 'no error for expired otp')
        self.assertFalse(resp.context['user'].is_authenticated, 'just tried the old otp')

    def try_random_otp(self):
        resp = self.post_password('AAA')
        self.assertIsNotNone(resp.context['error'], 'no error for random otp')
        self.assertFalse(resp.context['user'].is_authenticated, 'just tried a random otp')

    def try_valid_otp(self, otp):
        resp = self.post_password(otp.password)
        self.assertIsNone(resp.context.get('error'), 'something goes wrong with valid otp')
        user = resp.context['user']
        self.assertTrue(user.is_authenticated, 'user is not authenticated')
        self.assertEqual(user, self.user, 'must login with the created user')
        updated_otp = OTP.objects.get(pk=otp.pk)
        self.assertTrue(updated_otp.expired, 'expired does not change after use')

    def try_logout(self):
        resp = self.client.post(reverse('task:logout'), follow=True)
        self.assertIsNone(resp.context.get('error'), 'something goes wrong with logout')
        self.assertIsNotNone(resp.context.get('success'), 'no success message')
        user = resp.context['user']
        self.assertFalse(user.is_authenticated, 'user does not logout')

    def test_login(self):
        self.try_bad_email_login()
        self.try_login()
        valid_otp = self.get_generated_otp()
        self.check_new_otp(valid_otp)
        self.try_old_otp()
        self.try_random_otp()
        self.try_valid_otp(valid_otp)
        self.try_logout()

class SignupTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username='seth@example.org',
            email='seth@example.org'
        )
        self.signed_up = False

    def try_require_signup_view(self, path: str):
        resp = self.client.get(path, follow=True)
        self.assertEqual(len(resp.redirect_chain), 1)
        final, _ = resp.redirect_chain[0]
        self.assertEqual(final, reverse('task:signup'))

    def try_signup(self):
        resp = self.client.post(
            reverse('task:signup'),
            data={
                'firstname': 'seth',
                'lastname': 'seth'
            },
            follow=True
        )
        self.assertEqual(resp.status_code, 200)

    def check_for_taskuser(self):
        taskuser = TaskUser.from_user(self.user)
        if self.signed_up:
            return taskuser is not None
        else:
            return taskuser is None

    def test_signup(self):
        self.client.force_login(self.user)
        self.check_for_taskuser()
        for path in [
            reverse('task:index'),
            reverse('task:details', args=(12,)),
            reverse('task:add-task')
        ]:
            self.try_require_signup_view(path)
        self.try_signup()
        self.check_for_taskuser()
