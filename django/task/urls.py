from django.urls import path

from . import views

app_name = 'task'
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:task_id>/', views.details, name='details'),
    path('signup/', views.signup_view, name='signup'),
    path('logout/', views.logout_view, name='logout'),
    path('login/request/', views.login_request_view, name='login-request'),
    path('login/resolve/', views.login_resolve_view, name='login-resolve'),
    path('add_task/', views.add_task, name='add-task'),
]