import functools

from django.contrib.auth.decorators import user_passes_test
from django.views.decorators.http import (
    require_GET, require_http_methods, require_POST
)
from django.core.exceptions import PermissionDenied

from .models import OTP, Task, TaskUser, User

require_full_signup_or_none = user_passes_test(
    lambda user: not user.is_authenticated or TaskUser.from_user(user) is not None,
    login_url='task:signup',
    redirect_field_name=None,
)

require_incomplete_signup = user_passes_test(
    lambda user: user.is_authenticated and TaskUser.from_user(user) is None,
    login_url='task:index',
    redirect_field_name=None
)

def require_is_master(func):
    @functools.wraps(func)
    def wrapper(request, *args, **kwargs):
        user = request.user
        taskuser = TaskUser.from_user(user)
        if taskuser is None or not taskuser.is_master:
            raise PermissionDenied
        return func(request, *args, **kwargs)
    return wrapper

def require_not_authenticated(func):
    @functools.wraps(func)
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated:
            raise PermissionDenied
        return func(request, *args, **kwargs)
    return wrapper
