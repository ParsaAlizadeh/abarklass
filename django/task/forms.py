from datetime import timedelta

from django import forms
from django.core.exceptions import ValidationError
from django.utils import timezone

from .models import Task

class AddTaskForm(forms.ModelForm):
    class Meta:
        model = Task
        exclude = ('created_at', 'author')

    def clean_value(self):
        estimated = self.cleaned_data.get('estimated_hours')
        value = self.cleaned_data.get('value')
        if estimated is not None and value is not None and estimated < 72 and value > 50:
            self.add_error('value', 'maximum values for less than 3 days is 50$')
        return value

    def clean_deadline(self):
        now = timezone.now()
        deadline = self.cleaned_data.get('deadline')
        estimated = self.cleaned_data.get('estimated_hours')
        if estimated is None or deadline is None:
            return deadline
        if deadline < now + timedelta(hours=estimated):
            self.add_error('deadline', 'deadline and estimated hours doesnt match')
        return deadline

class LoginRequestForm(forms.Form):
    email = forms.EmailField()

class LoginResolveForm(forms.Form):
    password = forms.CharField(min_length=10, max_length=10)

class SignupForm(forms.Form):
    firstname = forms.CharField(max_length=100, strip=True)
    lastname = forms.CharField(max_length=100, strip=True)
    is_master = forms.BooleanField(required=False)
