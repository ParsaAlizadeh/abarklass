from datetime import timedelta

from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.duration import duration_string
from django.utils.timesince import timeuntil

class TaskUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    is_master = models.BooleanField(default=False)

    @classmethod
    def from_user(cls, user):
        if user.is_anonymous:
            return None
        return cls.objects.filter(user=user).first()

class Task(models.Model):
    title = models.CharField(max_length=255)
    value = models.IntegerField()
    estimated_hours = models.IntegerField()
    deadline = models.DateTimeField()
    author = models.ForeignKey(TaskUser, on_delete=models.CASCADE)
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'author={self.author.user.username} title="{self.title}"'

class OTP(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    password = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    expired = models.BooleanField()

    @classmethod
    def get_valids(cls, user):
        return cls.objects.filter(
            user=user,
            expired=False,
            created_at__gte=timezone.now() - timedelta(hours=24)
        )
