#!/bin/bash

mkdir -p /tmp/backups 
pg_dump -U sandbox abarklass > /tmp/backups/backup_$(date +'%Y-%m-%d_%H:%M').sql    # keep backup in a place far from database :)
