def is_coverd(string: str, pat: str) -> bool:
    return string.startswith(pat) and string.endswith(pat)

def html_tag(tag: str, content: str) -> str:
    return f'<{tag}>{content}</{tag}>'

with open('sample.csv') as sample:
    data = [
        [col.strip() for col in row.split(',')]
        for row in sample.readlines()
    ]

html = ['<table>']
for n_row, row in enumerate(data):
    cell = 'th' if n_row == 0 else 'td'
    html.append('<tr>')
    for col in row:
        content = col.strip()
        if is_coverd(col, '*'):
            content = html_tag('strong', col[1:-1])
        if is_coverd(col, '_'):
            content = html_tag('u', col[1:-1])
        html.append(html_tag(cell, content))
    html.append('</tr>')
html.append('</table>')

print('\n'.join(html))