import psycopg2
from pprint import pprint
from faker import Faker

faker = Faker()

conn = psycopg2.connect(
    user='sandbox',
    password='123',
    host='localhost',
    port='5432',
    database='abarklass',
)

def create_table_user():
    with conn.cursor() as curs:
        curs.execute(
            'create table if not exists users('
                'id serial primary key not null,'
                'name text not null,'
                'email text not null,'
                'age int not null'
            ');'
        )

def insert_fake_user():
    def generate_user():
        return faker.name(), faker.email(), faker.random_int(10, 50)

    with conn.cursor() as curs:
        curs.executemany(
            'insert into users(name, email, age) values (%s, %s, %s);',
            (generate_user() for _ in range(1000))
        )

def create_table_task():
    with conn.cursor() as curs:
        curs.execute(
            'create table if not exists tasks('
                'id serial primary key not null,'
                'title text not null,'
                'estimated_hours int not null,'
                'value int not null,'
                'deadline timestamp not null,'
                'user_id int not null references users(id) on delete cascade'
            ');'
        )

def insert_fake_task():
    with conn.cursor() as curs:
        curs.execute('select id from users;')
        ids = [row[0] for row in curs.fetchall()]
        generate_task = lambda : (
            faker.sentence(),
            faker.random_int(1, 8760),
            faker.random_int(10, 1000),
            faker.date_time_between('now', '+5y'),
            faker.random_element(ids)
        )
        curs.executemany(
            'insert into tasks(title, estimated_hours, value, deadline, user_id)'
            'values (%s, %s, %s, %s, %s);',
            (generate_task() for _ in range(1000))
        )

def task_order_by_deadline():
    with conn.cursor() as curs:
        curs.execute(
            'select u.name, t.title, t.deadline '
            'from tasks t '
            'join users u on t.user_id = u.id '
            'order by t.deadline '
            'limit 10;'
        )
        for rec in curs:
            pprint(rec)

def sum_value_per_user():
    with conn.cursor() as curs:
        curs.execute(
            'select u.id, u.name, sum(t.value) '
            'from tasks t '
            'join users u on t.user_id = u.id '
            'group by u.id;'
        )
        for rec in curs:
            pprint(rec)

def add_is_active_column():
    with conn.cursor() as curs:
        curs.execute(
            'alter table users '
            'add column is_active boolean not null default true;'
        )

def update_is_active_age_range():
    with conn.cursor() as curs:
        curs.execute(
            'update users '
            'set is_active = false '
            'where age < 18;'
        )

def create_table_friendship():
    with conn.cursor() as curs:
        curs.execute(
            'create table if not exists friendship('
                'id serial primary key not null,'
                'user_id1 int not null references users(id) on delete cascade,'
                'user_id2 int not null references users(id) on delete cascade'
            ');'
        )

def insert_fake_friendship():
    with conn.cursor() as curs:
        curs.execute('select id from users;')
        ids = [row[0] for row in curs.fetchall()]
        friendships = []
        for _ in range(500):
            id1, id2 = faker.random_elements(ids, length=2, unique=True)
            friendships.append((id1, id2))
        curs.executemany(
            'insert into friendship(user_id1, user_id2) values (%s, %s);',
            friendships
        )

def users_friend_with_age_range():
    with conn.cursor() as curs:
        curs.execute(
            'select distinct u2.name '
            'from users u1 '
            'join friendship f on u1.id = f.user_id1 '
            'join users u2 on u2.id = f.user_id2 '
            'where u1.age > 18;'
        )
        for rec in curs:
            pprint(rec)

with conn:
    users_friend_with_age_range()

conn.close()